K = [2,5,10,20,25,50,100];
K_main = 100;
filename = "CA-HepPh.txt";
outname = "CA-HepPh.output.txt";
% filename = "CA-GrQc.txt";
% outname = "CA-GrQc.output.txt";
% filename = "ca-CondMat.txt";
% outname = "ca-CondMat.output.txt";


fid = fopen(filename);
firstline = fgetl(fid);
%parsing the n
stlined = strsplit(firstline);
n = str2double(stlined(3));

tline = fgetl(fid);
A = sparse(n,n); %allocating memory for the array
while ischar(tline)
    line = sscanf(tline, '%d');
    A(line(1)+1,line(2)+1) = 1;
    A(line(2)+1,line(1)+1) = 1;
    tline = fgetl(fid);
end

[n,m] = size(A);
fclose(fid);
G = graph(A);

D = spdiags(degree(G),0,n,n);

L = D - A;
nL = sparse(n,n);
nL = sparse(spdiags((degree(G)).^(-.5),0,n,n))*L*(spdiags((degree(G)).^(-.5),0,n,n));
%%
for k = K

[eVectors,eValues] = eigs(nL,k, 0); %finds the k smallest eigenVectors

%10 nollaa ja 1 eroaa niin 10 clusteria
[d, ind] = sort(diag(eValues));
eValues = eValues(ind,ind);
eVectors = eVectors(:,ind);
U = eVectors(:,1:k);

u = sqrt( sum( U.^2, 2 ) );
Du = diag(u.^(-1));
Y0 = Du*U;

Y  = Y0-median(Y0);
%
goodness_best = 10^6;
I_best        = [];

%%
for i = 1:3
    [I, M]      = kmeans(Y,k,'distance','cosine');
    [a,b] = histc(I,unique(I));
    minV = min(a(b));
    [onesi,onesj,s] = find(A);
    cuts = 0;
    for i = 1:numel(onesi)
        if I(onesi(i)) ~= I(onesj(i))
            cuts = cuts + 1;
        end
    end
    
    cuts = cuts / 2;
    goodness = cuts / minV;
    
    if goodness < goodness_best
        goodness_best = goodness;
        I_best = I;
    end
end

k
goodness_best
minV
n

if k == K_main
    %Writing to file
    vertexID = 0:(n-1);
    result = [vertexID; I_best.'];
    fileID = fopen(outname,'w');
    fprintf(fileID,'%s\n',firstline);
    nbytes = fprintf(fileID,'%d %d\n',result);
    fclose(fileID);
end

end