CONTENT OF THE FOLDER:

- balanced.m uses the Balanced spectral graph partitioning algorithm
- basic.m uses the Basic spectral graph partitioning algorithm
- baseline.m randomly assigns cluster IDs for network vertices
- failed.m tries to balance the partitioning with quadratic integer program(NOTE: You must install OPTI toolbox from https://inverseproblem.co.nz/OPTI/index.php)


HOW TO RUN:
1. Find the code in directory /code. 
2. Open the .m-file of your liking in Matlab inside a directory that contains all the necessary graph files.
3. Run the script.


TIPS:
On top of each file you can find variable k that can be modified and several filenames. Uncomment the filename you'd like to use and comment the others.
