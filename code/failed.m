%% THIS ALGORITHM USES OPTI TOOLBOX TO MINIMIZE THE COST
% OF CLUSTER ASSIGNMENTS. If you wish to try this algorithm
% you must install this toolbox from https://inverseproblem.co.nz/OPTI/index.php

k = 2;
filename = "ca-GrQc.txt";
% filename = "ca-CondMat.txt";
% filename = "ca-AstroPh.txt";
%filename  ="ca-HepTh.txt";
fid = fopen(filename);
firstline = fgetl(fid);
%parsing the n
stlined = strsplit(firstline);
n = str2double(stlined(3));

tline = fgetl(fid);
A = sparse(n,n); %allocating memory for the array
while ischar(tline)
    line = sscanf(tline, '%d');
    A(line(1)+1,line(2)+1) = 1;
    A(line(2)+1,line(1)+1) = 1;
    tline = fgetl(fid);
end

% This algorithm really only works for small graphs. Try to simulate a
% graph with the following lines of code, for example.
% n_nodes = 100;
% A = zeros(n_nodes,n_nodes);
% n_edges = 3;
% rand_edges = randi(n_nodes,[n_nodes,n_edges]);
% for i = 1:n_nodes
%     if i < n_nodes
%         A(i,i+1) = 1;
%         A(i+1,i) = 1;
%     else
%         A(1,i) = 1;
%         A(i,1)   = 1;
%     end
%     for j = 1:n_edges
%         j_node = rand_edges(i,j);
%         if j_node ~= i
%             A(i,j_node) = 1;
%             A(j_node,i) = 1;
%         end
%     end
% end

[n,m] = size(A);
fclose(fid);
G = graph(A);

%D = diag(degree(G))

D = spdiags(degree(G),0,n,n);
% divdiag = spdiags(ones(n),0,n,n)
% sparse(divdiag./D)

L = D - A;
nL = sparse(n,n);
nL = sparse(spdiags((degree(G)).^(-.5),0,n,n))*L*(spdiags((degree(G)).^(-.5),0,n,n));

[eVectors,eValues] = eigs(nL,k, 0); %finds the k smallest eigenVectors

%10 nollaa ja 1 eroaa niin 10 clusteria
[d, ind] = sort(diag(eValues));
eValues = eValues(ind,ind);
eVectors = eVectors(:,ind);
U = eVectors(:,1:k);

u = sqrt( sum( U.^2, 2 ) );
Du = diag(u.^(-1));
Y0 = Du*U;

Y  = Y0-median(Y0);
%%
%nitialize Z and M
n_kmeans = 5;
% 
goodness_before_best     = 10^6;
I0_best                  = [];
M0_best                  = [];
for ik=1:n_kmeans
    [I0, M0]      = kmeans(Y,k,'distance','cosine');
    [a,b] = histc(I0,unique(I0));
    minV_before = min(a(b));
    [onesi,onesj,s] = find(A);
    cuts_before = 0;
    for i = 1:numel(onesi)
        if I0(onesi(i)) ~= I0(onesj(i))
            cuts_before = cuts_before + 1;
        end
    end
    
    cuts_before = cuts_before / 2;
    goodness_before = cuts_before / minV_before;
    
    if goodness_before < goodness_before_best
        goodness_before_best = goodness_before;
        I0_best     = I0;
        M0_best     = M0;
    end
end
I0      = I0_best;
M0      = M0_best;
%%
Z0 = sparse(n,k);
for i = 1:n
    Z0(i,I0(i)) = 1;
    Z_init(i,mod(i,k)+1) = 1;
end
Z0_vec       = Z0(:);                 % Zvec on nk x 1 vektori, jossa aina n kerrallaan% kukin Z:n kolumni                     
Z_init_vec   = Z_init(:);                                    

tmp         = 1:n*k;
k_ind       = reshape(tmp,[n,k]);  %matrix to store which element of Zvec corresponds to Z

%Objective function and constraints for fmincon                          
A_con       = sparse(k,n*k);
for j = 1:k
    A_con(j,k_ind(:,j)) = -1;
end
A_eq_con     = sparse(n,n*k);
for i = 1:n
   A_eq_con(i,k_ind(i,:)) = 1;
end
b_eq_con    = ones(n,1);

lb          = zeros(n*k,1);
ub          = ones(n*k,1);

a           = 0.9;
b_con        = -a*n/k*ones(k,1);

M   = [mean(Y)+std(Y);
       mean(Y)-std(Y)]
M       = M0;
MM = zeros(n*k,n*k);

for h = 1:k
for i = 1:n
    for j = 1:k
        MM((h-1)*n+i,(j-1)*n+i) = M(j,h);
    end
end
end
YY = Y(:);

% Integer Constraints - We do it as above (using repmat) in your case
xtype = repmat('B',[1,n*k]);

H   = -(1/2)*MM'*MM;
f   = -2*YY'*MM;
% Create OPTI Object
Opt = opti('qp',H,f,'ineq',A_con,b_con,'eq',A_eq_con,b_eq_con,'lb',lb,'ub',ub,'xtype',xtype);

% Solve the MIQP problem
tic
[Z_vec,fval,exitflag,info] = solve(Opt);
toc
Z       = reshape(Z_vec,[n,k]);
N = diag(sum(Z));
M = inv(N)*(Z')*Y;
%% conductance before
[a_before,b] = histc(I0,unique(I0));
minV_before = min(a_before(b));
[onesi,onesj,s] = find(A);
cuts_before = 0;
for i = 1:numel(onesi)
    if I0(onesi(i)) ~= I0(onesj(i))
        cuts_before = cuts_before + 1;
    end
end
cuts_before = cuts_before / 2;
goodness_before = cuts_before / minV_before

[val,loc] = max(Z');
I = loc';

%%Conductante after 
[a_after,b] = histc(I,unique(I));
minV = min(a_after(b));

[onesi,onesj,s] = find(A);

cuts = 0;
for i = 1:numel(onesi)
    if I(onesi(i)) ~= I(onesj(i))
        cuts = cuts + 1;
    end
end
cuts = cuts / 2;
goodness_after = cuts / minV

%Writing to file
vertexID = 0:(n-1);
result = [vertexID; I.'];
fileID = fopen('results.txt','w');
fprintf(fileID,'%s\n',firstline);
nbytes = fprintf(fileID,'%d %d\n',result);
fclose(fileID);

