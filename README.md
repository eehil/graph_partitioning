# Graph partitioning project

## Introduction

Graph partitioning has various applications, such as knowledge discovery (e.g., parti-
tioning the web into sets of related pages or finding groups of related queries submitted
in a search engine) and performance improvement (i.e., partitioning nodes of a large so-
cial network into different machines so that macro-parallelization can be used) (Gionis,
2018).

In general, the discrete problem of partitioning a graph optimally is NP-hard (Chung,
1997). However, methods can be used to obtain a guaranteed approximation of the opti-
mal cut (Spielman and Teng, 1996). One approach for this approximation is to augment
the clustering problem of the partitioning task with spectral graph theory, which is the
study and exploration of graphs through the eigenvalues of matrices naturally associ-
ated with those graphs (Spielman, 2012). In these spectral graph partitioning methods,
the top eigenvectors of a matrix are used for clustering (Ng et al., 2001).

In this project, we design and implement a graph partitioning method, and use this
method in partitioning various data sets from the Stanford Network Analysis Project
(SNAP) 1 .