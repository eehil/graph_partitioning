k = 100;
%filename = "ca-GrQc.txt";
%filename = "test.txt";
%filename = "ca-CondMat.txt";
%filename = "ca-AstroPh.txt";
%filename  ="ca-HepPh.txt";
%filename  ="ca-HepTh.txt";
%filename = "Oregon-1.txt";
%filename = "soc-Epinions1.txt";
%filename = "web-NotreDame.txt";
fid = fopen(filename);
firstline = fgetl(fid);
firstline = strcat(firstline,strcat(" ", int2str(k)));
%parsing the n
stlined = strsplit(firstline);
outputfile = strcat(stlined(2), ".output");
n = str2double(stlined(3));
tline = fgetl(fid);
tic();
A = spalloc(n,n,str2double(stlined(4))); %allocating memory for the array
while ischar(tline)
    line = sscanf(tline, '%d');
    A(line(1)+1,line(2)+1) = 1;
    A(line(2)+1,line(1)+1) = 1;
    tline = fgetl(fid);
end
toc()
I = randi([0 k-1],1,n);

[a,b] = histc(I,unique(I));
minV = min(a(b));

[onesi,onesj,s] = find(A);


cuts = 0;
for i = 1:numel(onesi)
    if I(onesi(i)) ~= I(onesj(i))
        cuts = cuts + 1;
    end
end
cuts = cuts / 2;
goodness = cuts / minV



%%Writing to file
vertexID = 0:(n-1);
result = [vertexID; I];
fileID = fopen(outputfile,'w');
fprintf(fileID,'%s\n',firstline);
nbytes = fprintf(fileID,'%d %d\n',result);
fclose(fileID);